class Friend
  def greeting(who = nil)
    who = ", #{who}" if who != nil
    "Hello#{who}!"
  end
end
