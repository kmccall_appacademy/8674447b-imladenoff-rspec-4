class Timer
    attr_accessor :seconds

  def initialize
    @seconds = 0
  end

  def time_string
    second_trunc = (seconds % 60).to_s
    second_trunc = "0#{second_trunc}" if second_trunc.length == 1
    mins_trunc = ((seconds % 60**2) / 60).to_s
    mins_trunc = "0#{mins_trunc}" if mins_trunc.length == 1
    hours_trunc = (seconds / 60 / 60).to_s
    hours_trunc = "0#{hours_trunc}" if hours_trunc.length == 1
    "#{hours_trunc}:#{mins_trunc}:#{second_trunc}"
  end
end
