PREPS = ["in", "of"]
DETS = ["the", "a", "an"]
CONJS = ["and", "or"]

class Book
    attr_accessor :title

  def title= title
    title_arr = title.split.map.with_index do |word, i|
      case
      when i == 0
        word.capitalize
      when (PREPS+DETS+CONJS).include?(word)
        word
      else
        word.capitalize
      end
    end
    @title = title_arr.join " "
  end
end
