class Dictionary
  attr_reader :entries

  def initialize
    @entries = {}
  end

  def add addition
    if addition.is_a?(Hash)
      @entries.merge! addition
    elsif addition.is_a?(String)
      @entries[addition] = nil
    end
  end

  def keywords
    @entries.keys.sort
  end

  def include? keyword
    @entries.has_key?(keyword)
  end

  def find term
    @entries.select { |k,v| k.match term }
  end

  def printable
    prints = []
    @entries.each { |k,v| prints << %Q~[#{k}] "#{v}"~ }
    prints.sort.join"\n"
  end
end
