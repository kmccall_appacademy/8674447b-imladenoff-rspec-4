class Temperature
  def initialize(options)
    if options[:f]
      self.fahrenheit = options[:f]
    else
      self.celsius = options[:c]
    end
  end

  def fahrenheit=(temp)
    @temperature = temp
  end

  def celsius=(temp)
    @temperature = (temp * (9.0/5)) + 32
  end

  def in_fahrenheit
    @temperature
  end

  def in_celsius
    (@temperature - 32) * (5.0/9)
  end

  def self.from_celsius(temp)
    self.new(c: temp)
  end

  def self.from_fahrenheit(temp)
    self.new(f: temp)
  end

end

class Celsius < Temperature
  def initialize(temp)
    self.celsius = temp
  end
end

class Fahrenheit < Temperature
  def initialize(temp)
    self.fahrenheit = temp
  end
end
